# -*-coding:utf-8 -*-
import re
import shutil
import os

# 获取工作路径
print("请输入你放置图片文件夹的名称: ")
print("注意，如果路径含有特殊符号需要用「\」转义")
_input = input()



# 创建 replace _trash_bin文件夹
if os.path.exists('./replace') == False:
  os.mkdir('./replace')
  print("创建文件夹成功：replace")
else:
  raise Exception("请删除当前目录下的 replace 文件夹后重试")

if os.path.exists('./_trash_bin') == False:
  os.mkdir('./_trash_bin')
  print("创建文件夹成功：_trash_bin")
else:
  raise Exception("请删除当前目录下的 _trash_bin 文件夹后重试")

# 撰写正则表达式
# pattern = re.compile('\(.*img\/.*\..*\)')
# pattern = re.compile('\(' + _input + '\/.*\..*\)')
pattern = re.compile('\!\[.*\]\(' + _input + '\/.*\)')


# 创建需要被移动的文件列表
find_list = []

# 获取当前路径下的 .md 文件
md_finder = os.listdir('./')
md_list = []
for item in md_finder:
  # 寻找 markdown 文件
  if item.endswith('.md') == True:
    md_list.append(item)

# 逐个读取 .md 文件
for md_item in md_list:
  f = open(md_item)
  md_str = f.read()
  f.close()
  result = pattern.findall(md_str)
  for i in range(len(result)):
    # 将图片路径逐个添加至 find_list
    index = result[i].find("]")

    # 将拿到的原始图片路径加工成 ./<imgs_name>/xxx.png 的形式
    add_item = "./" + _input + "/" + result[i][index + 3 + len(_input): len(result[i]) - 1]

    space_index = add_item.find("%20")
    # 如果路径中存在 空格符
    if space_index != -1:
      add_item = add_item[0: space_index] + " " + add_item[space_index + 3:]

    # 将图片路径添加至 find_list
    find_list.append(add_item)

# 将所有 markdown中 引用过路径的图片移动至 ./replace
for item in find_list:
  shutil.move(item, './replace')
  print(item + " 移动完成")

print("----------------------")

# 将没有引用的图片移至 _trash_bin
img_trashs = os.listdir('./' + _input)
trash_flag = 0
for item_trash in img_trashs:
  item_trash = "./" + _input + "/" + item_trash
  trash_flag = trash_flag + 1
  shutil.move(item_trash, './_trash_bin')
  print(item_trash + " in _trash_bin")

print("----------------------")

# 删除原本的 img 文件夹，并更改文件夹名称
os.removedirs('./' + _input + '/')

os.rename('./replace', _input)

print(str(trash_flag) + " 个 未引用")
print("移动成功！没有引用的图片已经移入至 _trash_bin")
